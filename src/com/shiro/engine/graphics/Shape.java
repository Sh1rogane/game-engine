package com.shiro.engine.graphics;

import java.awt.Color;

public abstract class Shape extends Graphic
{
	protected Color fillColor;
	protected Color outlineColor;
	
	public Shape(Color color)
	{
		this(color, color);
	}
	public Shape(Color fill, Color outline)
	{
		fillColor = fill;
		outlineColor = outline;
	}
	public void setFillColor(Color c)
	{
		this.fillColor = c;
	}
	public void setOutlineColor(Color c)
	{
		this.outlineColor = c;
	}
}
