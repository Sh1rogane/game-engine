package com.shiro.engine.graphics;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.HashMap;

import com.shiro.engine.Time;

public class Animation extends Image
{
	private HashMap<String, int[]> animations = new HashMap<>();

	private int frameWidth;
	private int frameHeight;
	private double frameTime;

	private double currentTime;

	private Rectangle[] r;

	private int currentFrame = 0;
	private boolean playing;
	private boolean repeat;
	private boolean rewind;

	private String currentAnimation = "";
	private int[] ca;

	public Animation(String spritesheet, int frameWidth, int frameHeight, double frameTime)
	{
		super(spritesheet);
		this.frameWidth = frameWidth;
		this.frameHeight = frameHeight;
		this.frameTime = frameTime;

		int xFrames = image.getWidth() / this.frameWidth;
		int yFrames = image.getHeight() / this.frameHeight;

		r = new Rectangle[xFrames * yFrames];

		int frame = 0;
		// Gets where all the frames
		for(int y = 0; y < yFrames; y++)
		{
			for(int x = 0; x < xFrames; x++)
			{
				r[frame] = new Rectangle(x * frameWidth, y * frameHeight, x * frameWidth
						+ frameWidth, y * frameHeight + frameHeight);
				frame++;
			}

		}
	}
	public void addAnimation(String name, int... frames)
	{
		animations.put(name, frames);
	}
	public void play(boolean repeat)
	{
		rewind = false;
		playing = true;
		this.repeat = repeat;
	}
	public void rewind(boolean repeat)
	{
		rewind = true;
		playing = true;
		this.repeat = repeat;
	}
	public boolean isPlaying()
	{
		return playing;
	}
	public String getCurrentAnimation()
	{
		return currentAnimation;
	}
	public int getTotalFrames()
	{
		return r.length;
	}
	public int getCurrentFrame()
	{
		return currentFrame;
	}
	public void play(int frame, boolean repeat)
	{
		this.currentFrame = frame;
		play(repeat);
	}
	public void play(String animation, boolean repeat)
	{
		if(currentAnimation.equals(animation))
		{
			play(currentFrame, repeat);
		}
		if(!currentAnimation.equals(animation) || !playing)
		{
			currentAnimation = animation;
			ca = animations.get(animation);
			play(0, repeat);
		}
	}
	public void stop()
	{
		playing = false;
	}
	public void stop(int frame)
	{
		this.currentFrame = frame;
		stop();
	}
	@Override
	public void render(Graphics2D g, int width, int height)
	{
		if(playing)
		{
			if(rewind)
				this.currentTime -= Time.getRenderDeltaTime();
			else
				this.currentTime += Time.getRenderDeltaTime();

			if(!rewind && currentTime >= frameTime)
			{
				currentTime = 0;
				currentFrame++;

				// Change frame
				if(currentAnimation.isEmpty())
				{
					if(currentFrame >= r.length)
					{
						if(repeat)
						{
							currentFrame = 0;
						}
						else
						{
							currentFrame = r.length - 1;
						}
					}
				}
				else
				{
					if(currentFrame >= ca.length)
					{
						currentFrame = 0;
						if(repeat)
						{
							currentFrame = 0;
						}
						else
						{
							currentFrame = ca.length - 1;
						}
					}
				}
			}
			else if(rewind && currentTime <= 0)
			{
				currentTime = frameTime;
				currentFrame--;

				// Change frame
				if(currentAnimation.isEmpty())
				{
					if(currentFrame < 0)
					{
						if(repeat)
						{
							currentFrame = r.length - 1;
						}
						else
						{
							currentFrame = 0;
						}
					}
				}
				else
				{
					if(currentFrame < 0)
					{
						currentFrame = 0;
						if(repeat)
						{
							currentFrame = ca.length - 1;
						}
						else
						{
							currentFrame = 0;
						}
					}
				}
			}
		}
		if(currentAnimation.isEmpty())
		{
			g.drawImage(image, 0, 0, width, height, r[currentFrame].x, r[currentFrame].y,
					r[currentFrame].width, r[currentFrame].height, null);
		}
		else
		{
			g.drawImage(image, 0, 0, width, height, r[ca[currentFrame]].x, r[ca[currentFrame]].y,
					r[ca[currentFrame]].width, r[ca[currentFrame]].height, null);
		}
	}
}
