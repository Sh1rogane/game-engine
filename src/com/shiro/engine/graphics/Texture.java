package com.shiro.engine.graphics;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;

import com.shiro.engine.Loader;

public class Texture extends Graphic
{
	private BufferedImage image;
	private TexturePaint tp;

	public Texture(String textureFile)
	{
		image = Loader.getImage(textureFile);
		tp = new TexturePaint(image, new Rectangle(image.getWidth(), image.getHeight()));
	}
	@Override
	public void render(Graphics2D g, int width, int height)
	{
		if(image == null)
		{
			super.render(g, width, height);
		}
		else
		{
			g.setPaint(tp);
			g.fillRect(0, 0, width, height);
		}
	}
}
