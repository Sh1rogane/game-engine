package com.shiro.engine.graphics;

import java.awt.Color;
import java.awt.Graphics2D;

public class Box extends Shape
{
	public Box(Color color)
	{
		this(color, color);
	}
	public Box(Color fillColor, Color outlineColor)
	{
		super(fillColor, outlineColor);
	}
	@Override
	public void render(Graphics2D g, int width, int height)
	{
		g.setColor(fillColor);
		if(this.width == 0 && this.height == 0)
			g.fillRect(offsetX, offsetY, width, height);
		else
			g.fillRect(offsetX, offsetY, this.width, this.height);
		g.setColor(outlineColor);
		if(this.width == 0 && this.height == 0)
			g.drawRect(offsetX, offsetY, width, height);
		else
			g.drawRect(offsetX, offsetY, this.width, this.height);
	}
}
