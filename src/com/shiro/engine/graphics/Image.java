package com.shiro.engine.graphics;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.shiro.engine.Loader;

public class Image extends Graphic
{
	protected BufferedImage image;
	protected boolean flipX = false;
	protected boolean flipY = false;
	
	public Image(String image)
	{
		this.image = Loader.getImage(image);
		this.width = this.image.getWidth();
		this.height = this.image.getHeight();
	}

	@Override
	public void render(Graphics2D g, int width, int height)
	{
		if(image == null)
		{
			super.render(g, width, height);
		}
		else
		{
			g.drawImage(image, (width * (flipX ? 1 : 0)), (height * (flipY ? 1 : 0)), width * (flipX ? -1 : 1), height * (flipY ? -1 : 1), null);
		}
	}
	public void setFlip(boolean x, boolean y)
	{
		this.flipX = x;
		this.flipY = y;
	}
}
