package com.shiro.engine.graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class Text extends Graphic
{
	private String text;
	private Color color;
	private Font font;

	public Text(String text, Color color, Font font)
	{
		this.text = text;
		this.color = color;
		this.font = font;
	}
	@Override
	public void render(Graphics2D g, int width, int height)
	{
		g.setColor(color);
		g.setFont(font);
		g.drawString(text, offsetX, offsetY);
	}
}
