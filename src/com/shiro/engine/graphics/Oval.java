package com.shiro.engine.graphics;

import java.awt.Color;
import java.awt.Graphics2D;

public class Oval extends Shape
{
	public Oval(Color color)
	{
		this(color, color);
	}
	public Oval(Color fillColor, Color outlineColor)
	{
		super(fillColor, outlineColor);
	}
	@Override
	public void render(Graphics2D g, int width, int height)
	{
		g.setColor(fillColor);
		if(this.width == 0 && this.height == 0)
			g.fillOval(offsetX, offsetY, width, height);
		else
			g.fillOval(offsetX, offsetY, this.width, this.height);
		g.setColor(outlineColor);
		if(this.width == 0 && this.height == 0)
			g.drawOval(offsetX, offsetY, width, height);
		else
			g.drawOval(offsetX, offsetY, this.width, this.height);
	}
}
