package com.shiro.engine.graphics;

import java.awt.Color;
import java.awt.Graphics2D;

public class Graphic
{
	protected boolean render = true;
	protected int width = 0;

	protected int height = 0;
	protected int offsetX = 0;
	protected int offsetY = 0;
	
	public Graphic()
	{

	}
	public void setVisibility(boolean value)
	{
		this.render = value;
	}
	public void render(Graphics2D g, int width, int height)
	{
		g.setColor(Color.MAGENTA);
		if(this.width == 0 && this.height == 0)
			g.fillRect(offsetX, offsetY, width, height);
		else
			g.fillRect(offsetX, offsetY, this.width, this.height);
		g.setColor(Color.WHITE);
		if(this.width == 0 && this.height == 0)
			g.drawRect(offsetX, offsetY, width, height);
		else
			g.fillRect(offsetX, offsetY, this.width, this.height);
	}
	public boolean getVisibility()
	{
		return render;
	}
	public int getWidth()
	{
		return width;
	}
	public void setWidth(int width)
	{
		this.width = width;
	}
	public int getHeight()
	{
		return height;
	}
	public void setHeight(int height)
	{
		this.height = height;
	}
	public int getOffsetX()
	{
		return offsetX;
	}
	public void setOffsetX(int offsetX)
	{
		this.offsetX = offsetX;
	}
	public int getOffsetY()
	{
		return offsetY;
	}
	public void setOffsetY(int offsetY)
	{
		this.offsetY = offsetY;
	}
}
