package com.shiro.engine;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;

import com.shiro.engine.components.ColliderComponent;
import com.shiro.engine.components.PhysicsComponent;
import com.shiro.engine.components.RenderComponent;
import com.shiro.engine.components.ScriptComponent;
import com.shiro.engine.components.SoundComponent;
import com.shiro.engine.components.TransformComponent;
import com.shiro.engine.gameobjects.Entity;

public class Scene
{
	private ArrayList<Entity> entityList = new ArrayList<>();
	private ArrayList<Entity> removeList = new ArrayList<>();
	private ArrayList<Entity> addList = new ArrayList<>();

	private static Scene currentScene;
	private static Scene changeToScene;
	private static boolean changeScene;

	public static Scene getCurrentScene()
	{
		return currentScene;
	}
	// Change the scene to specified
	public static void setCurrentScene(Scene scene)
	{
		if(currentScene == null)
			currentScene = scene;
		else
		{
			changeToScene = scene;
			changeScene = true;
		}
	}

	public Scene()
	{

	}
	// Code that runs first time
	public void init()
	{

	}
	// Clears the scene from entitys and clears the physics world
	public void clear()
	{
		for(Entity e : entityList)
		{
			e.removeSelf();
		}
		entityList.clear();
		Engine.getPhysicsWorld().clearBodys();
	}
	// Returns the first entity with the tag
	public Entity getEntity(String tag)
	{
		for(Entity e : entityList)
		{
			if(e.getTag().equals(tag))
				return e;
		}
		return null;
	}
	// Returns all entitys with the tag
	public ArrayList<Entity> getEntitys(String tag)
	{
		ArrayList<Entity> list = new ArrayList<>();
		if(tag.isEmpty())
			return entityList;

		for(Entity e : entityList)
		{
			if(e.getTag().equals(tag))
				list.add(e);
		}
		return list;
	}
	public void tick()
	{
		// Adds and removes Entitiys from the entity list
		this.addRemove();
		for(Entity e : entityList)
		{
			// Gets and update TransformComponent
			TransformComponent tc = e.getComponent(TransformComponent.class);
			if(tc != null)
			{
				tc.tick();
			}
			// Gets and update all ScriptComponents
			ArrayList<ScriptComponent> s = e.getScriptComponents();
			for(ScriptComponent sc : s)
			{
				sc.tick();
			}
			ColliderComponent cc = e.getComponent(ColliderComponent.class);
			if(cc != null)
			{
				cc.tick();
			}
			// Gets and update PhysicsComponent
			PhysicsComponent pc = e.getComponent(PhysicsComponent.class);
			if(pc != null)
			{
				pc.tick();
			}
			// Gets and update SoundComponent
			SoundComponent sc = e.getComponent(SoundComponent.class);
			if(sc != null)
			{
				sc.tick();
			}
		}
		if(changeScene)
		{
			changeScene = false;
			if(currentScene != null)
			{
				currentScene.clear();
			}
			currentScene = changeToScene;
			currentScene.init();
		}
	}
	// Returns all entitys
	public ArrayList<Entity> getEntitys()
	{
		return entityList;
	}
	public void add(Entity e)
	{
		addList.add(e);
		// Call init e all scripts
		ArrayList<ScriptComponent> s = e.getScriptComponents();
		for(ScriptComponent sc : s)
		{
			sc.init();
		}
	}
	public void remove(Entity e)
	{
		removeList.add(e);
	}
	private void addRemove()
	{
		if(addList.size() > 0)
		{
			entityList.addAll(addList);
			addList.clear();
			sortByLayer();
		}

		if(removeList.size() > 0)
		{
			entityList.removeAll(removeList);
			removeList.clear();
		}

	}
	private void sortByLayer()
	{
		Collections.sort(entityList);
	}
	public void render(Graphics2D g, double i)
	{
		for(Entity e : entityList)
		{
			RenderComponent rc = e.getComponent(RenderComponent.class);
			if(rc != null)
			{
				rc.render(g, i);
			}
		}
	}
}
