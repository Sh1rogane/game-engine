package com.shiro.engine;

import com.shiro.engine.components.TransformComponent;
import com.shiro.engine.gameobjects.Entity;

public class Camera
{
	private static double x = 0;
	private static double y = 0;

	public static double oldX = 0;
	public static double oldY = 0;

	private static int width = 0;
	private static int height = 0;

	private static double boundX = 0;
	private static double boundY = 0;

	private static double deadZoneX = 0;
	private static double deadZoneY = 0;
	
	private static double offsetX = 0;
	private static double offsetY = 0;

	private static boolean lockX = false;
	private static boolean lockY = false;

	private static double smooth = 1;

	private static TransformComponent followTc;
	
	private static double shakeDuration = 0;
	private static double shakeX = 0;
	private static double shakeY = 0;

	public Camera(int width, int height)
	{
		Camera.width = width;
		Camera.height = height;
	}
	public static int getWidth()
	{
		return width;
	}
	public static int getHeight()
	{
		return height;
	}
	public static void shake(double duration, double xFactor, double yFactor)
	{
		shakeDuration = duration;
		shakeX = xFactor;
		shakeY = yFactor;
	}
	// Sets how much easing it should be
	public static void setSmoothness(double value)
	{
		smooth = value;
	}
	// Lock or unlock y-axis
	public static void lockY(boolean value)
	{
		lockY = value;
	}
	// Lock or unlock x-axis
	public static void lockX(boolean value)
	{
		lockX = value;
	}
	// Sets the deadzone where camera does not follows
	public static void setDeadZone(double deadzoneX, double deadzoneY)
	{
		deadZoneX = deadzoneX;
		deadZoneY = deadzoneY;
	}
	public static void setOffset(double x, double y)
	{
		offsetX = x;
		offsetY = y;
	}
	// Sets the max bounds for the camera (world size)
	public static void setBounds(double x, double y)
	{
		boundX = x - width;
		boundY = y - height;
	}
	public static double getBoundX()
	{
		return boundX + width;
	}
	public static double getBoundY()
	{
		return boundY + height;
	}
	// Sets the camera to follow a entitys transform
	public static void setFollow(Entity e)
	{
		followTc = e.getComponent(TransformComponent.class);
	}
	// Sets the camera to follow a transform
	public static void setFollow(TransformComponent tc)
	{
		followTc = tc;
	}
	public static void removeFollow()
	{
		followTc = null;
	}
	public static void setPosition(double x, double y)
	{
		Camera.x = x - width / 2;
		Camera.y = y - height / 2;
		oldX = x;
		oldY = y;
	}
	public static double getX()
	{
		return x;
	}
	public static double getY()
	{
		return y;
	}
	public void tick()
	{
		// Save old position for smoothing/interpolation
		oldX = x;
		oldY = y;
		if(followTc != null)
		{
			// transform center
			double x1 = followTc.getX() + followTc.getCenter().x + offsetX;
			double y1 = followTc.getY() + followTc.getCenter().y + offsetY;

			// Cameras center
			double x2 = x + (width / 2);
			double y2 = y + (height / 2);

			// Relative position
			double xx = x1 - x2;
			double yy = y1 - y2;
			
			double mx = 0;
			double my = 0;

			// If not locked x and transform is not in deadzoneX
			if(!lockX && Math.abs(xx) > deadZoneX)
			{
				mx = (x1 - x2);
				if(xx > 0)
					mx -= deadZoneX;
				else
					mx += deadZoneX;
			}
			// If not locked y and transform is not in deadzoneY
			if(!lockY && Math.abs(yy) > deadZoneY)
			{
				my = (y1 - y2);
				if(yy > 0)
					my -= deadZoneY;
				else
					my += deadZoneY;
			}
			
			
			x += mx / smooth;
			y += my / smooth;
			
			// Limits the camera to the bounds
			if(x < 0)
				x = 0;
			else if(x > boundX)
				x = boundX;
			if(y < 0)
				y = 0;
			else if(y > boundY)
				y = boundY;
			
			if(shakeDuration > 0)
			{
				shakeDuration -= Time.getDeltaTime();
				x += (Math.random() * shakeX * 2 - shakeX) / smooth;
				y += (Math.random() * shakeY * 2 - shakeY) / smooth;
			}
			
		}
	}
}
