package com.shiro.engine;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JFrame;

import com.shiro.engine.components.ScriptComponent;
import com.shiro.engine.gameobjects.Entity;
import com.shiro.physics.World;

public class Engine extends Canvas implements Runnable, ComponentListener
{
	private static final long serialVersionUID = 1L;

	private final long SKIP_TICKS = (1000 * 1000 * 1000) / 30; // 30 fps

	private int windowWidth = 0;
	private int windowHeight = 0;
	private int gameWidth = 0;
	private int gameHeight = 0;
	private double gameScale = 1;

	private int gameWindowX = 0;
	private int gameWindowY = 0;

	private JFrame frame;

	private BufferStrategy bs;

	private boolean running;

	private Input input = new Input();

	private static World world = new World();

	static GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment()
			.getDefaultScreenDevice();

	private JFrame fullScreen;
	private int frameWidth = 100;
	private int frameHeight = 100;
	private boolean isFullscreen;

	private static Engine instance;

	private Camera camera;

	private static BufferedImage hideMouse = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
	public static Cursor hideCursor = Toolkit.getDefaultToolkit().createCustomCursor(hideMouse, new Point(0, 0), "hide cursor");
	public static void setMouse(Cursor cursor)
	{
		instance.setCursor(cursor);
	}
	// Returns the physics world
	public static World getPhysicsWorld()
	{
		return world;
	}
	// Sets the game in fullscreen or not
	public static void setFullscreen(boolean value)
	{
		instance.setfullscreen(value);
	}
	// Returns if the game is in fullscreen
	public static boolean isFullsceen()
	{
		return instance.isFullscreen;
	}
	// Returns the current scale of the game
	public static double getScale()
	{
		return instance.gameScale;
	}
	// Returns the "viewport" x
	public static int getGameX()
	{
		return instance.gameWindowX;
	}
	// Returns the "viewport" y
	public static int getGameY()
	{
		return instance.gameWindowY;
	}
	public static int getGameWidth()
	{
		return instance.gameWidth;
	}
	public static int getGameHeight()
	{
		return instance.gameHeight;
	}
	public Engine(int windowWidth, int windowHeight, int gameWidth, int gameHeight)
	{
		super(device.getDefaultConfiguration());
		instance = this;
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		this.gameWidth = gameWidth;
		this.gameHeight = gameHeight;
		camera = new Camera(gameWidth, gameHeight);

		this.setIgnoreRepaint(true);

		Scene.setCurrentScene(new Scene());

		setupFrame();
		if(!init())
		{
			System.out.println("Could not init Engine!");
			System.exit(1);
		}
	
	}
	// Setting up the container and if it is a window pack it
	private boolean setupFrame()
	{
		System.setProperty("sun.java2d.opengl", "True");
		
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		fullScreen = new JFrame();
		fullScreen.setUndecorated(true);
		fullScreen.setIgnoreRepaint(true);
		
		this.setSize(windowWidth, windowHeight);
		this.setPreferredSize(new Dimension(windowWidth, windowHeight));

		frame.setSize(windowWidth, windowHeight);
		frame.setIgnoreRepaint(true);

		frame.add(this, BorderLayout.CENTER);
		frame.revalidate();

		this.addMouseListener(input);
		this.addMouseMotionListener(input);
		this.addKeyListener(input);

		this.addComponentListener(this);

		frame.setVisible(true);

		frame.requestFocus();
		this.requestFocusInWindow();

		this.resizeGame(frame.getWidth(), frame.getHeight());

		return true;
	}
	// Creates the BufferStrategy
	private boolean init()
	{
		this.createBufferStrategy(2);
		bs = this.getBufferStrategy();
		if(bs != null)
			return true;

		return false;
	}
	// Startig the game loop
	public void start()
	{
		System.out.println("Start engine!");
		this.running = true;
		
		Thread t = new Thread(this);
		t.start();
	}
	

	@Override
	public void run()
	{
		int fps = 0;
		int tps = 0;

		long lastTime = Time.getTickCount();
		long nextTick = Time.getTickCount();

		Time.setTickStart(Time.getTickCount());
		Time.setTickEnd(Time.getTickCount());
		Time.setLoopEnd(Time.getTickCount());

		Time.setTimeScale(1);

		final int MAX_SKIP = 3;

		while(running)
		{
			try
			{
				// MAX FPS 500
				Thread.sleep(2);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			Time.setLoopStart(Time.getTickCount());

			int loops = 0;

		

			// Loop until nextTick is greater than currentTick
			while(Time.getTickCount() > nextTick && loops < MAX_SKIP)
			{
				Time.setTickStart(Time.getTickCount());
				// Update all tick methods
				this.tick();
				// Add SKIP_TICKS to nextTick to set when next tick should run
				nextTick += SKIP_TICKS;
				loops++;
				tps++;
				// System.out.println(Time.getDeltaTime());
				Time.setTickEnd(Time.getTickStart());
			}
			world.update(120);
			// Calculates where we are in the tick on the render side(0.0 - 1.0)
			double interpolation = (Time.getTickCount() + SKIP_TICKS - nextTick)
					/ (double) SKIP_TICKS;
			
			// Calls render with interpolation value
			this.render(interpolation);

			// Once per second print fps and tps
			fps++;
			if(Time.getTickCount() - lastTime > 1000 * 1000 * 1000)
			{
				lastTime += 1000 * 1000 * 1000;
				System.out.println("Ticks: " + tps + " FPS: " + fps);
				frame.setTitle("Ticks: " + tps + " FPS: " + fps);
				tps = 0;
				fps = 0;
			}
			Time.setLoopEnd(Time.getLoopStart());
		}
	}
	private void tick()
	{
		Scene.getCurrentScene().tick();
		if(Input.getKeyTyped(KeyEvent.VK_ESCAPE))
		{
			setFullscreen(false);
		}
		if(Input.getKeyTyped(KeyEvent.VK_F11))
		{
			setFullscreen(true);
		}
		camera.tick();
		input.tick();
	}
	private void render(double i)
	{
		Graphics2D g = null;
		try
		{
			// Gets the graphics from BufferStratrgy
			g = (Graphics2D) bs.getDrawGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			AffineTransform af = g.getTransform();
			// Scales and centers the game
			g.translate(gameWindowX, gameWindowY);
			g.scale(gameScale, gameScale);
			
			// Clears the screen
			g.clearRect(0, 0, gameWidth, gameHeight);
			g.setColor(Color.BLACK);
			// Fill the screen with black
			g.fillRect(0, 0, gameWidth, gameHeight);
			
			// Translate where the camera is
			g.translate(-(Camera.oldX + i * (Camera.getX() - Camera.oldX)), -(Camera.oldY + i
					* (Camera.getY() - Camera.oldY)));
			
			//world.debugDraw(g, Color.cyan);
			// Render all
			Scene.getCurrentScene().render(g, i);
			
			g.setTransform(af);
			af = g.getTransform();
			// Scales and centers the game
			g.translate(gameWindowX, gameWindowY);
			g.scale(gameScale, gameScale);
			
			//Render UI
			for(Entity e : Scene.getCurrentScene().getEntitys())
			{
				// Gets and update all ScriptComponents drawUI
				ArrayList<ScriptComponent> s = e.getScriptComponents();
				for(ScriptComponent sc : s)
				{
					sc.drawUI(g);
				}
			}
			g.setTransform(af);
			// The blacks bar when scaling
			g.setColor(Color.DARK_GRAY);
			g.fillRect(0, 0, gameWindowX, this.getHeight());
			g.fillRect(0, 0, this.getWidth(), gameWindowY);
			g.fillRect(this.getWidth() - gameWindowX, 0, gameWindowX, this.getHeight());
			g.fillRect(0, this.getHeight() - gameWindowY, this.getWidth(), gameWindowY);
			// And show the graphic
			bs.show();
		}
		catch(IllegalStateException e)
		{
			// If BufferStrategy not exist create it again
			e.printStackTrace();
			this.createBufferStrategy(2);
			bs = this.getBufferStrategy();
		}
		finally
		{
			if(g != null)
				g.dispose();
		}
		// Sync graphics with display
		//getToolkit().sync();
	}
	// Sets the game to fullscreen
	private void setfullscreen(boolean value)
	{
		if(value && !isFullscreen)
		{
			isFullscreen = true;
			frameWidth = this.getWidth();
			frameHeight = this.getHeight();
			fullScreen.add(this);
			frame.remove(this);
			frame.setVisible(false);
			device.setFullScreenWindow(fullScreen);
			fullScreen.requestFocus();
			this.requestFocusInWindow();
			this.resizeGame(fullScreen.getWidth(), fullScreen.getHeight());
			fullScreen.setVisible(true);
		}
		else if(!value && isFullscreen)
		{
			isFullscreen = false;
			device.setFullScreenWindow(null);
			fullScreen.remove(this);
			frame.add(this);
			frame.setVisible(true);
			fullScreen.setVisible(false);
			this.resizeGame(frameWidth, frameHeight);
		}
	}
	// Calculats the new scale and position for the game
	private void resizeGame(int newWidth, int newHeight)
	{
		this.setSize(newWidth, newHeight);
		this.setPreferredSize(new Dimension(newWidth, newHeight));

		double newWRatio = newWidth / (double) gameWidth;
		double newHRatio = newHeight / (double) gameHeight;
		gameScale = Math.min(newWRatio, newHRatio);

		gameWindowX = (int) ((newWidth - (gameWidth * gameScale)) / 2);
		gameWindowY = (int) ((newHeight - (gameHeight * gameScale)) / 2);

		frame.pack();
	}
	@Override
	public void componentHidden(ComponentEvent e)
	{
	}
	@Override
	public void componentMoved(ComponentEvent e)
	{
	}
	@Override
	public void componentResized(ComponentEvent e)
	{
		resizeGame(e.getComponent().getWidth(), e.getComponent().getHeight());
	}
	@Override
	public void componentShown(ComponentEvent e)
	{
	}
}
