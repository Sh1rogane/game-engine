package com.shiro.engine;

public class Time
{
	private static long startTick = 0;
	private static long endTick = 0;
	private static long startLoop = 0;
	private static long endLoop = 0;

	private static double timeScale = 1;

	public Time()
	{

	}
	public void reset()
	{

	}
	public static long getTickCount()
	{
		return System.nanoTime();
	}
	protected static long getTickStart()
	{
		return startTick;
	}
	protected static void setLoopStart(long time)
	{
		startLoop = time;
	}
	protected static long getLoopStart()
	{
		return startLoop;
	}
	protected static void setLoopEnd(long time)
	{
		endLoop = time;
	}
	protected static void setTickStart(long time)
	{
		startTick = time;
	}
	protected static void setTickEnd(long time)
	{
		endTick = time;
	}
	public static double getDeltaTime()
	{
		return (startTick - endTick) / 1000000000.0 * timeScale;
	}
	public static double getDeltaTimeRaw()
	{
		return (startTick - endTick) / 1000000000.0;
	}
	public static double getRenderDeltaTime()
	{
		return (startLoop - endLoop) / 1000000000.0 * timeScale;
	}
	public static double getRenderDeltaTimeRaw()
	{
		return (startLoop - endLoop) / 1000000000.0;
	}
	public static double getTimeScale()
	{
		return timeScale;
	}
	public static void setTimeScale(double timeScale)
	{
		Time.timeScale = timeScale;
		Engine.getPhysicsWorld().setTimeScale(timeScale);
	}
}
