package com.shiro.engine.components;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import com.shiro.engine.Camera;
import com.shiro.engine.gameobjects.Entity;
import com.shiro.engine.graphics.Graphic;

public class RenderComponent extends Component
{
	private static Graphic noGraphic = new Graphic();
	private ArrayList<Graphic> graphics = new ArrayList<>();
	private TransformComponent tc;
	private int layer = 0;

	public RenderComponent(Entity owner, Graphic graphic)
	{
		super(owner);
		this.graphics.add(graphic);
		tc = owner.getComponent(TransformComponent.class);
	}
	public RenderComponent(Entity owner, Graphic graphic, int layer)
	{
		this(owner, graphic);
		this.setLayer(layer);
	}
	public void addGraphic(Graphic g)
	{
		this.graphics.add(g);
	}
	public void cleanUp()
	{
		// for(Graphic g : graphics)
		// {
		// //g.setVisibility(false);
		// }
	}
	// Temp solution for interpolation rotation
	private double shortestRotation()
	{
		double ar = tc.getRotation();
		double aor = tc.oldRotation;
		double r = ar - aor;
		if(Math.abs(r) > 300)
		{
			return 0;
		}
		return Math.toRadians(r);
	}
	private boolean inside(TransformComponent tc)
	{
		double minX1 = tc.getX() - 100;
		double maxX1 = tc.getX() + tc.getWidth() + 100;
		double minX2 = Camera.getX();
		double maxX2 = Camera.getX() + Camera.getWidth();
		double minY1 = tc.getY() - 100;
		double maxY1 = tc.getY() + tc.getHeight() + 100;
		double minY2 = Camera.getY();
		double maxY2 = Camera.getY() + Camera.getHeight();
		if(maxX1 < minX2 || maxY1 < minY2 || minX1 > maxX2 || minY1 > maxY2)
		{
			return false;
		}
		return true;
	}
	public void render(Graphics2D g, double i)
	{
		if(tc != null)
		{
			// Saves the transform before changing it
			AffineTransform af = g.getTransform();
			// Translate the graphic to the TransformComponet position and
			// interpolates it.
			g.translate(tc.oldX + i * (tc.getX() - tc.oldX), tc.oldY + i * (tc.getY() - tc.oldY));
			g.rotate(Math.toRadians(tc.oldRotation) + i * (shortestRotation()), tc.getCenter().x, tc.getCenter().y);
			// If transform is inside viewport render
			if(inside(tc))
			{
				// If graphics exist render it
				if(!graphics.isEmpty())
				{
					for(Graphic graphic : graphics)
					{
						if(graphic.getVisibility())
						{
							graphic.render(g, tc.getWidth(), tc.getHeight());
						}
					}
				}
				// If not render the default "no graphic" graphic
				else
				{
					noGraphic.render(g, tc.getWidth(), tc.getHeight());
				}
			}

			// Sets the transformation as it was before
			g.setTransform(af);
		}
	}
	public int getLayer()
	{
		return layer;
	}
	public void setLayer(int layer)
	{
		this.layer = layer;
	}
}
