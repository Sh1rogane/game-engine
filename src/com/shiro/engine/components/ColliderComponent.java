package com.shiro.engine.components;

import java.awt.geom.Rectangle2D;

import com.shiro.engine.Scene;
import com.shiro.engine.gameobjects.Entity;

public class ColliderComponent extends Component
{
	private TransformComponent tc;
		
	private int width;
	private int height;
	
	private double xOffset = 0;
	private double yOffset = 0;
	
	public ColliderComponent(Entity owner)
	{
		super(owner);
		tc = owner.getComponent(TransformComponent.class);
		width = tc.getWidth();
		height = tc.getHeight();
	}
	public void setSize(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	public void setOffset(double xOffset, double yOffset)
	{
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	public Rectangle2D.Double getHitbox()
	{
		return new Rectangle2D.Double(tc.getX() + xOffset, tc.getY() + yOffset, width, height);
	}
	public void tick()
	{
		// Loop all scene entitys
		for(Entity e : Scene.getCurrentScene().getEntitys())
		{
			ColliderComponent c = e.getComponent(ColliderComponent.class);
			// Check if e has a collider
			if(c != null && c != this)
			{
				Rectangle2D h = this.getHitbox();
				Rectangle2D h2 = c.getHitbox();
				if(h.intersects(h2))
				{
					for(ScriptComponent sc : owner.getScriptComponents())
					{
						// Call all script component onCollision
						sc.onCollision(e);
					}
				}
			}
		}
	}
}
