package com.shiro.engine.components;

import com.shiro.engine.Engine;
import com.shiro.engine.gameobjects.Entity;
import com.shiro.physics.Body;
import com.shiro.physics.Shape;
import com.shiro.physics.Vector2;

public class PhysicsComponent extends Component
{
	// private double mass = 1;
	private double friction = 0.5;
	public double restitution = 1;

	private TransformComponent tc;
	// private boolean _static;

	Body body;

	public PhysicsComponent(Entity owner, Shape shape, boolean _static)
	{
		super(owner);
		// this._static = _static;
		tc = owner.getComponent(TransformComponent.class);

		if(tc == null)
			System.err.println("Entity has no Transform component");

		body = new Body(shape, tc.getX(), tc.getY(), friction, restitution, _static);
		Engine.getPhysicsWorld().addBody(body);
	}
	@Override
	public void cleanUp()
	{
		Engine.getPhysicsWorld().removeBody(body);
	}
	public void addPos(double x, double y)
	{
		body.setX(body.getX() + x);
		body.setY(body.getY() + y);
		body.resetVelocity();
	}
	public void setFriction(double friction)
	{
		body.setFriction(friction);
	}
	public void setRestitution(double restitution)
	{
		body.setRestitution(restitution);
	}
	public void setPos(double x, double y)
	{
		body.setX(x);
		body.setY(y);
		body.resetVelocity();
	}
	public Vector2 getVeclocity()
	{
		return body.getVelocity();
	}
	public void setVelocityX(double x)
	{
		body.setVelocityX(x);
	}
	public void setVelocityY(double y)
	{
		body.setVelocityY(y);
	}
	public void setVelocity(double x, double y)
	{
		body.setVelocity(x, y);
	}
	public void addForceX(double force)
	{
		body.applyImpulse(force, 0);
	}
	public void addForceY(double force)
	{
		body.applyImpulse(0, force);
	}
	public void tick()
	{
		tc.setX(body.getX());
		tc.setY(body.getY());
	}
}
