package com.shiro.engine.components;

import java.awt.Point;
import java.awt.event.MouseEvent;

import com.shiro.engine.Input;
import com.shiro.engine.gameobjects.Entity;

public class TransformComponent extends Component
{
	private double x = 0;
	private double y = 0;
	private int width = 0;
	private int height = 0;

	private double rotation = 0;

	public double oldX = 0;
	public double oldY = 0;
	public double oldRotation = 0;
	
	//For mouse listener
	private MouseListener listener;
	private boolean onOver = false;
	private boolean onUp = true;
	private int centerX;
	private int centerY;

	public TransformComponent(Entity owner, double x, double y, int width, int height)
	{
		super(owner);
		this.x = x;
		this.y = y;
		this.oldX = x;
		this.oldY = y;
		this.width = width;
		this.height = height;
		this.centerX = width / 2;
		this.centerY = height / 2;
	}
	public void addOnMouseListener(MouseListener listener)
	{
		this.listener = listener;
	}
	// Saves the old position for interpolation reasons
	public void tick()
	{
		this.oldX = x;
		this.oldY = y;
		this.oldRotation = rotation;
		
		if(listener != null)
		{
			if(!onUp && !Input.getMouseDown(MouseEvent.BUTTON1))
			{
				onUp = true;
				listener.onUp(owner);
			}
			if(contains(Input.getMouseX(), Input.getMouseY()) && Input.getMouseTyped(MouseEvent.BUTTON1))
			{
				onUp = false;
				listener.onClicked(owner);
			}
			if(contains(Input.getMouseX(), Input.getMouseY()) && Input.getMouseDown(MouseEvent.BUTTON1))
			{
				onUp = false;
				listener.onDown(owner);
			}
			if(!onOver && contains(Input.getMouseX(), Input.getMouseY()))
			{
				onOver = true;
				listener.onOver(owner);
			}
			else if(onOver && !contains(Input.getMouseX(), Input.getMouseY()))
			{
				onOver = false;
				listener.onOut(owner);
			}
		}
	}
	public double getX()
	{
		return x;
	}
	public void setXNow(double x)
	{
		this.x = x;
		this.oldX = x;
	}
	public void setYNow(double y)
	{
		this.y = y;
		this.oldY = y;
	}
	public void setX(double x)
	{
		this.x = x;
	}
	public void addX(double add)
	{
		this.x += add;
	}
	public double getY()
	{
		return y;
	}
	public void setY(double y)
	{
		this.y = y;
	}
	public void addY(double add)
	{
		this.y += add;
	}
	public int getWidth()
	{
		return width;
	}
	public void setWidth(int width)
	{
		this.width = width;
	}
	public int getHeight()
	{
		return height;
	}
	public void setHeight(int height)
	{
		this.height = height;
	}
	// Returns true if transform is inside
	public boolean contains(TransformComponent tc)
	{
		if(this.x + this.width > tc.getX() && this.x < tc.getX() + tc.getWidth()
				&& this.y + this.height > tc.getY() && this.y < tc.getY() + tc.getHeight())
		{
			return true;
		}

		return false;
	}
	public boolean contains(double x, double y)
	{
		if(x > this.x && x < this.x + this.width && y > this.y && y < this.y + this.height)
		{
			return true;
		}
		return false;
	}
	public double getRotation()
	{
		return rotation;
	}
	public void setRotation(double rotation)
	{
		if(rotation > 359)
			rotation -= 359;
		if(rotation < 0)
			rotation += 359;
		
		this.rotation = rotation;
	}
	public void setCenter(int centerX, int centerY)
	{
		this.centerX = centerX;
		this.centerY = centerY;
	}
	public Point getCenter()
	{
		return new Point(centerX, centerY);
	}
}
