package com.shiro.engine.components;

import com.shiro.engine.gameobjects.Button;

public interface ButtonListener
{
	public void onClicked(Button button);
}
