package com.shiro.engine.components;

import com.shiro.engine.gameobjects.Entity;

public interface MouseListener
{	
	public void onClicked(Entity entity);
	public void onDown(Entity entity);
	public void onUp(Entity entity);
	public void onOver(Entity entity);
	public void onOut(Entity entity);
}
