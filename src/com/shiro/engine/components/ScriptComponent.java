package com.shiro.engine.components;

import java.awt.Graphics2D;

import com.shiro.engine.gameobjects.Entity;

public class ScriptComponent extends Component
{
	private Script scriptInterface;

	public ScriptComponent(Entity owner, Script scriptInterface)
	{
		super(owner);
		this.scriptInterface = scriptInterface;
	}
	public ScriptComponent(Entity owner)
	{
		this(owner, null);
	}
	// Code that runs when created
	public void init()
	{
		if(scriptInterface != null)
		{
			scriptInterface.init();
		}
	}
	// Code that runs each tick
	public void tick()
	{
		if(scriptInterface != null)
		{
			scriptInterface.tick();
		}
	}
	public void drawUI(Graphics2D g)
	{
		if(scriptInterface != null)
		{
			scriptInterface.drawUI(g);
		}
	}
	public void onCollision(Entity collisonWith)
	{
		if(scriptInterface != null)
		{
			scriptInterface.onCollision(collisonWith);
		}
	}
	public void recieve(String message)
	{
		if(scriptInterface != null)
		{
			scriptInterface.recieve(message);
		}
	}
}
