package com.shiro.engine.components;

import com.shiro.engine.gameobjects.Entity;

public abstract class Component
{
	protected Entity owner;

	public Component(Entity owner)
	{
		this.owner = owner;
	}
	// Used for clean up
	public void cleanUp()
	{

	}
	public void recieve(String message)
	{

	}
}
