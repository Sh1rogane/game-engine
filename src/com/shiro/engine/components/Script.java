package com.shiro.engine.components;

import java.awt.Graphics2D;

import com.shiro.engine.gameobjects.Entity;

public interface Script
{
	public void init();
	public void tick();
	public void onCollision(Entity e);
	public void drawUI(Graphics2D g);
	public void recieve(String message);
}
