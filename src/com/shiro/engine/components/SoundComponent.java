package com.shiro.engine.components;

import com.shiro.engine.Camera;
import com.shiro.engine.MathUtils;
import com.shiro.engine.gameobjects.Entity;
import com.shiro.engine.sounds.Sound;

public class SoundComponent extends Component
{
	private Sound sound;
	private TransformComponent tc;
	private TransformComponent otherTc;
	private double power = 500;
	
	private double volume = 1;

	public SoundComponent(Entity owner, Sound sound, TransformComponent tc)
	{
		super(owner);
		this.sound = sound;
		this.otherTc = tc;

		this.tc = owner.getComponent(TransformComponent.class);

	}
	public void setVolume(double value)
	{
		this.volume = value;
	}
	public void setSound(Sound sound)
	{
		this.sound = sound;
	}
	public void setPower(double power)
	{
		this.power  = power;
	}
	public void play()
	{
		// I = P/(4 pi r^2) where P is the effect, and r is the distance
		double pos = tc.getX() - otherTc.getX();
		// Should be gameWidth
		pos /= Camera.getWidth();

		double dist = MathUtils.distance(tc.getX(), tc.getY(), otherTc.getX(), otherTc.getY());

		// System.out.println(1 - Math.abs(pos));
		double I = power / (3.14 * 2 * (dist / 100) * (dist / 100));

		// System.out.println(I);
		if(I < 0.2)
			I = 0;
		if(I > 1)
			I = 1;
		
		sound.setVolume((float) (I * volume));

		sound.setPan((float) pos * 2.0f);
		sound.play(false);
		
	}
	@Override
	public void cleanUp()
	{
		// sound.stop();
	}
	public void tick()
	{
		// I = P/(4 pi r^2) where P is the effect, and r is the distance
		double pos = tc.getX() - otherTc.getX();
		// Should be gameWidth
		pos /= 800;

		double dist = MathUtils.distance(tc.getX(), tc.getY(), otherTc.getX(), otherTc.getY());

		// System.out.println(1 - Math.abs(pos));
		double I = 500.0 / (3.14 * 2 * (dist / 100) * (dist / 100));

		// System.out.println(I);
		if(I < 0.2)
			I = 0;

		sound.setVolume((float) I);

		sound.setPan((float) pos * 2.0f);
	}
}
