package com.shiro.engine;

import com.shiro.engine.components.TransformComponent;

public class MathUtils
{
	public static double distance(double x1, double y1, double x2, double y2)
	{
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}
	public static double distance(TransformComponent tc1, TransformComponent tc2)
	{
		return distance(tc1.getX(), tc1.getY(), tc2.getX(), tc2.getY());
	}
	public static double radToDeg(double radian)
	{
		double deg = (180 / Math.PI) * radian;
		return deg;
	}
	public static double degToRad(double angle)
	{
		double rad = (Math.PI / 180) * angle;
		return rad;
	}	
	public static double calcRotation(double x1, double x2, double y1, double y2)
	{
		double xx = x1 - x2;
		double yy = y1 - y2;
		return radToDeg(Math.atan2(yy, xx));
	}
	public static double shortestRotation(double from, double to)
	{
		double r1 = from;
		double r2 = to;
		double r3 = r2 - r1;
		
		if(r3 > 180)
			r3 -= 360;
		if(r3 < -180)
			r3 += 360;
		
		return r3;
	}
}
