package com.shiro.engine.sounds;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.shiro.engine.Loader;
import com.shiro.engine.components.TransformComponent;

public class Sound
{
	private AudioInputStream stream;
	private Clip soundClip;

	private FloatControl volumeControl;
	private FloatControl panControl;
	
	private Sound slowSound;

	private static TransformComponent ears;
	
	private Sound(AudioInputStream stream, Clip clip)
	{
		this.stream = stream;
		this.soundClip = clip;
		volumeControl = (FloatControl) soundClip.getControl(FloatControl.Type.MASTER_GAIN);
		panControl = (FloatControl) soundClip.getControl(FloatControl.Type.PAN);
	}
	public Sound(String audioFile)
	{
		try
		{
			Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
			Mixer mixer = AudioSystem.getMixer(mixerInfo[0]);

			stream = AudioSystem.getAudioInputStream(Sound.class.getResourceAsStream(audioFile));

			AudioFormat format = stream.getFormat();
			DataLine.Info line = new DataLine.Info(Clip.class, format);

			soundClip = (Clip) mixer.getLine(line);
			soundClip.open(stream);
			volumeControl = (FloatControl) soundClip.getControl(FloatControl.Type.MASTER_GAIN);
			panControl = (FloatControl) soundClip.getControl(FloatControl.Type.PAN);
			
			slowSound = createSlower(0.5, audioFile);
		}
		catch(Exception e)
		{
			System.err.println(e);
		}
	}
	public void setVolume(float volume)
	{
		if(volume < 0)
		{
			volume = 0;
		}
		else if(volume > 1)
		{
			volume = 1;
		}
		float dB = (float) (Math.log(volume) / Math.log(10.0) * 20.0);
		volumeControl.setValue(dB);
		if(slowSound != null)
			slowSound.setVolume(volume);
	}
	public void setPan(float pan)
	{
		if(pan < -1)
		{
			pan = -1;
		}
		else if(pan > 1)
		{
			pan = 1;
		}
		panControl.setValue(pan);
		if(slowSound != null)
			slowSound.setPan(pan);
	}
	public void playSlow()
	{
		slowSound.play(false);
	}
	public void play(boolean loop)
	{
		if(!loop)
		{
			//if(soundClip.isRunning())
				soundClip.stop();

			soundClip.setFramePosition(0);
			soundClip.start();
		}
		else
		{
			soundClip.loop(Clip.LOOP_CONTINUOUSLY);
		}
	}
	public void stop()
	{
		soundClip.stop();
	}

	public static Sound getSound(String sound)
	{
		return Loader.getSound(sound);
	}

	public static void setEars(TransformComponent tc)
	{
		ears = tc;
	}
	public static TransformComponent getEars()
	{
		return ears;
	}

	private Sound createSlower(double value, String name)
	{
		try
		{
			double playBackSpeed = value;
			
			AudioInputStream ais = AudioSystem.getAudioInputStream(Sound.class.getResourceAsStream(name));

			AudioFormat af = ais.getFormat();

			int frameSize = af.getFrameSize();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] b = new byte[2 ^ 16];
			int read = 1;
			while(read > -1)
			{
				read = ais.read(b);
				if(read > 0)
				{
					baos.write(b, 0, read);
				}
			}
			byte[] b1 = baos.toByteArray();
			byte[] b2 = new byte[(int) (b1.length / playBackSpeed)];
			int a = (int) (1 / playBackSpeed);
			int aa = 0;
			for(int ii = 0; ii < b2.length / frameSize; ii++)
			{
				if(ii % a == 0)
				{
					aa++;
				}
				for(int jj = 0; jj < frameSize; jj++)
				{
					if((aa * frameSize) + jj < b1.length)
						b2[(ii * frameSize) + jj] = b1[(aa * frameSize) + jj];

				}
			}
			ByteArrayInputStream bais = new ByteArrayInputStream(b2);
			AudioInputStream aisAccelerated = new AudioInputStream(bais, af, b2.length);
			Clip clip;

			clip = AudioSystem.getClip();
			clip.open(aisAccelerated);
			
			return new Sound(aisAccelerated, clip);
		}
		catch(LineUnavailableException | IOException | UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
