package com.shiro.engine.gameobjects;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;

import com.shiro.engine.Engine;
import com.shiro.engine.components.ButtonListener;
import com.shiro.engine.components.MouseListener;
import com.shiro.engine.components.RenderComponent;
import com.shiro.engine.components.TransformComponent;
import com.shiro.engine.graphics.Box;
import com.shiro.engine.graphics.Image;
import com.shiro.engine.graphics.Text;

public class Button extends Entity implements MouseListener
{
	private TransformComponent tc;
	private RenderComponent rc;
	private ButtonListener bl;
	
	public Button(double x, double y, int width, int height, String text, ButtonListener bl)
	{
		this.bl = bl;
		tc = new TransformComponent(this, x, y, width, height);
		tc.addOnMouseListener(this);
		this.addComponent(tc);
		rc = new RenderComponent(this, new Box(Color.CYAN));
		rc.addGraphic(new Text(text, Color.WHITE, new Font("Arial", Font.PLAIN, 20)));
		this.addComponent(rc);
		
	}
	public Button(double x, double y, String image, ButtonListener bl)
	{
		Image i = new Image(image);
		this.bl = bl;
		tc = new TransformComponent(this, x, y, i.getWidth(), i.getHeight());
		tc.addOnMouseListener(this);
		this.addComponent(tc);
		rc = new RenderComponent(this, i);
		this.addComponent(rc);
		
	}
	@Override
	public void removeSelf()
	{
		super.removeSelf();
		Engine.setMouse(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	@Override
	public void onClicked(Entity entity)
	{
		// TODO Auto-generated method stub
		bl.onClicked(this);
	}

	@Override
	public void onDown(Entity entity)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUp(Entity entity)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onOver(Entity entity)
	{
		// TODO Auto-generated method stub
		Engine.setMouse(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	@Override
	public void onOut(Entity entity)
	{
		// TODO Auto-generated method stub
		Engine.setMouse(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
}
