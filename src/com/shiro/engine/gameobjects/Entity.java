package com.shiro.engine.gameobjects;

import java.util.ArrayList;
import java.util.HashMap;

import com.shiro.engine.Scene;
import com.shiro.engine.components.Component;
import com.shiro.engine.components.RenderComponent;
import com.shiro.engine.components.ScriptComponent;

public class Entity implements Comparable<Entity>
{
	private HashMap<Class<? extends Component>, Component> components = new HashMap<>();
	private String tag = "";

	public Entity()
	{

	}
	public void setTag(String tag)
	{
		this.tag = tag;
	}
	public String getTag()
	{
		return tag;
	}
	public void addComponent(Component c)
	{
		components.put(c.getClass(), c);
	}
	// Returns the component asked for
	public <T extends Component> T getComponent(Class<T> component)
	{
		return component.cast(components.get(component));
	}
	// Returns all ScriptComponetns
	public ArrayList<ScriptComponent> getScriptComponents()
	{
		ArrayList<ScriptComponent> scripts = new ArrayList<>();
		for(Component c : components.values())
		{
			if(c instanceof ScriptComponent)
			{
				scripts.add((ScriptComponent) c);
			}
		}
		return scripts;
	}
	// Sends a message to specified component
	public <T extends Component> void sendMessage(Class<T> component, String message)
	{
		Component c = getComponent(component);
		if(c != null)
		{
			c.recieve(message);
		}
		else
		{
			System.err.println("No such component exist!");
		}
	}
	// Remove self and calls clean up on all components
	public void removeSelf()
	{
		for(Component c : components.values())
		{
			c.cleanUp();
		}
		Scene.getCurrentScene().remove(this);
	}
	@Override
	public int compareTo(Entity o)
	{
		RenderComponent r1 = this.getComponent(RenderComponent.class);
		RenderComponent r2 = o.getComponent(RenderComponent.class);
		
		if(r1 != null && r2 != null)
		{
			return Integer.compare(r1.getLayer(), r2.getLayer());
		}
		return 0;
	}
}
