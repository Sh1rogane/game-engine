package com.shiro.engine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

//The Input class, which handles both mouse and keyboard input, by implementing some of the standard Java libraries 
//concerning key- and mouseEvents.

public class Input implements MouseListener, MouseMotionListener, KeyListener
{
	//class variables includes hashmaps to store the keyCodes, as well as the hashMap definedKeys,
	//which stores a string as a key with an integer[] (keycodes) as value. 
	//Other vars include integers for mouse positions, as well as booleans to see if they are pressed or held.

	private static Map<Integer, Boolean> keyDown = new HashMap<Integer, Boolean>();
	private static Map<Integer, Boolean> keyTyped = new HashMap<Integer, Boolean>();
	private static Map<String, int[]> definedKeys = new HashMap<String, int[]>();

	private static int mouseX = 0;
	private static int mouseY = 0;
	private static boolean leftButtonDown = false;
	private static boolean rightButtonDown = false;

	private static boolean leftButtonTyped = false;
	private static boolean rightButtonTyped = false;

	
	
	//the defineKeys method allows the user set an array of keyCodes to one string, which is saved in the definedKeys hashMap - for example, 
	//	"Jump" could be attributed to the spacebar and up key. 
	public static void defineKeys(String identifier, int... keyCodes)
	{
		definedKeys.put(identifier, keyCodes);

	}

	
	//the following 4 methods checks if a key is pressed or held. they have two variants, one where you give the correct keyCode directly, 
	//and one where it fetches the defined keys for the String argument.
	public static boolean getKeyDown(int keyCode)
	{
		Boolean b = keyDown.get(keyCode);
		if(b != null)
			return b;
		else
			return false;
	}

	public static boolean getKeyDown(String keyCode)
	{

		int i[] = definedKeys.get(keyCode);

		for(int j : i)
		{
			if(getKeyDown(j))
			{
				return true;
			}
		}
		return false;
	}

	public static boolean getKeyTyped(int keyCode)
	{
		Boolean b = keyTyped.get(keyCode);

		if(b != null)
			return b;
		else
			return false;
	}

	public static boolean getKeyTyped(String keyCode)
	{

		int i[] = definedKeys.get(keyCode);
		for(int j : i)
		{
			if(getKeyTyped(j))
			{
				return true;
			}
		}
		return false;
	}

	//these methods do the same as the above, except its for mouse buttons.
	public static boolean getMouseTyped(int button)
	{

		if(button == MouseEvent.BUTTON1)
		{
			return leftButtonTyped;
		}
		else if(button == MouseEvent.BUTTON3)
		{
			return rightButtonTyped;
		}

		return false;
	}

	public static boolean getMouseDown(int button)
	{

		if(button == MouseEvent.BUTTON1)
		{
			return leftButtonDown;
		}
		else if(button == MouseEvent.BUTTON3)
		{
			return rightButtonDown;
		}

		return false;

	}

	//fetches the cursors x and y positions, respectively.
	public static int getMouseX()
	{
		return (int) ((mouseX - Engine.getGameX()) / Engine.getScale() + Camera.getX());
	}

	public static int getMouseY()
	{

		return (int) ((mouseY - Engine.getGameY()) / Engine.getScale() + Camera.getY());
	}
	//fetches the cursor position
	public static Point2D getMouse()
	{

		return new Point2D.Double(getMouseX(), getMouseY());
	}

	
	//The following methods are overriden methods from the standard libraries for key-and mouseevents.
	@Override
	public void keyPressed(KeyEvent e)
	{
		Boolean b1 = getKeyDown(e.getKeyCode());
		if(b1 != null && b1 == false)
			keyTyped.put(e.getKeyCode(), true);

		keyDown.put(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		keyDown.put(e.getKeyCode(), false);
		keyTyped.put(e.getKeyCode(), false);
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		keyDown.put(e.getKeyCode(), false);
		keyTyped.put(e.getKeyCode(), false);
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		if(e.getButton() == MouseEvent.BUTTON1)
		{
			leftButtonTyped = true;
			leftButtonDown = true;
		}
		if(e.getButton() == MouseEvent.BUTTON3)
		{
			rightButtonTyped = true;
			rightButtonDown = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		if(e.getButton() == MouseEvent.BUTTON1)
		{
			leftButtonTyped = false;
			leftButtonDown = false;
		}
		if(e.getButton() == MouseEvent.BUTTON3)
		{
			rightButtonTyped = false;
			rightButtonDown = false;
		}
	}
	//tick method, clears keypress and mousepress everytick.
	public void tick()
	{
		keyTyped.clear();
		rightButtonTyped = false;
		leftButtonTyped = false;
	}

}
