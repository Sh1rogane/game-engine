package com.shiro.engine;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import com.shiro.engine.sounds.Sound;

public class Loader
{
	private static HashMap<String, BufferedImage> images = new HashMap<>();
	private static HashMap<String, Sound> sounds = new HashMap<>();

	// Load a image
	public static void loadImage(String file)
	{
		// Checks if image are not loaded
		if(images.get(file) == null)
		{
			BufferedImage res = null;
			try
			{
				// Loads the image from the location
				BufferedImage org = ImageIO.read(Loader.class.getResource(file));
				res = toCompatibleImage(org);

			}
			catch(IOException | IllegalArgumentException e)
			{
				System.err.println("Warning! Image not found: \"" + file
						+ "\" using replacement texture instead");
			}
			images.put(file, res);
		}
	}
	public static BufferedImage getImage(String file)
	{
		// Checks if the image is loaded if not load it
		if(images.get(file) == null)
		{
			loadImage(file);
		}
		// Return it
		return images.get(file);
	}
	public static void unloadImage(String file)
	{
		images.remove(file);
	}
	public static void loadSound(String file)
	{
		if(sounds.get(file) == null)
		{
			Sound sound = new Sound(file);
			sounds.put(file, sound);
		}
	}
	public static Sound getSound(String file)
	{
		// Checks if the image is loaded if not load it
		if(sounds.get(file) == null)
		{
			loadSound(file);
		}
		// Return it
		return sounds.get(file);
	}

	private static BufferedImage toCompatibleImage(BufferedImage image)
	{
		// obtain the current system graphical settings
		GraphicsConfiguration gfxConfig = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice().getDefaultConfiguration();

		// if image are optimized
		if(image.getColorModel().equals(gfxConfig.getColorModel()))
			return image;

		// image is not optimized
		BufferedImage newImage = gfxConfig.createCompatibleImage(image.getWidth(),
				image.getHeight(), image.getTransparency());

		// get the graphics context of the new image to draw the old image on
		Graphics2D g = (Graphics2D) newImage.getGraphics();

		// actually draw the image and dispose of context no longer needed
		g.drawImage(image, 0, 0, null);
		g.dispose();

		// return the new optimized image
		return newImage;
	}
}
