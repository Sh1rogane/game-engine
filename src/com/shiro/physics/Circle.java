package com.shiro.physics;

public class Circle extends Shape
{
	public double radius;

	public Circle(double radius)
	{
		this.radius = radius;
	}
	public Vector2 getCenter()
	{
		return new Vector2(body.position.x + radius, body.position.y + radius);
	}
	@Override
	public double calculateMass(double density)
	{
		return density * radius * radius * 3.14;
	}
	@Override
	public Type getType()
	{
		return Type.Circle;
	}
	public Vector2 getMin()
	{
		return new Vector2(body.position.x, body.position.y);
	}
	public Vector2 getMax()
	{
		return new Vector2(body.position.x + radius * 2, body.position.y + radius * 2);
	}

}
