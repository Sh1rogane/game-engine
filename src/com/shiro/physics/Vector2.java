package com.shiro.physics;

public class Vector2
{
	public double x;
	public double y;

	public Vector2()
	{
		this(0, 0);
	}
	public Vector2(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	public Vector2(Vector2 vector)
	{
		this.x = vector.x;
		this.y = vector.y;
	}
	public Vector2 copy()
	{
		return new Vector2(this);
	}
	public Vector2 reset()
	{
		this.x = 0;
		this.y = 0;
		return this;
	}
	public Vector2 set(double x, double y)
	{
		this.x = x;
		this.y = y;
		return this;
	}
	public Vector2 set(Vector2 vector)
	{
		return this.set(vector.x, vector.y);
	}
	public Vector2 add(double x, double y)
	{
		this.x += x;
		this.y += y;
		return this;
	}
	public Vector2 add(Vector2 vector)
	{
		return this.add(vector.x, vector.y);
	}
	public Vector2 addr(double x, double y)
	{
		Vector2 v = new Vector2(this);
		v.add(x, y);
		return v;
	}
	public Vector2 addr(Vector2 vector)
	{
		Vector2 v = new Vector2(this);
		v.add(vector);
		return v;
	}
	public Vector2 sub(double x, double y)
	{
		this.x -= x;
		this.y -= y;
		return this;
	}
	public Vector2 sub(Vector2 vector)
	{
		return this.sub(vector.x, vector.y);
	}
	public Vector2 subr(double x, double y)
	{
		Vector2 v = new Vector2(this);
		v.sub(x, y);
		return v;
	}
	public Vector2 subr(Vector2 vector)
	{
		Vector2 v = new Vector2(this);
		v.sub(vector);
		return v;
	}
	public Vector2 mul(double vx, double vy)
	{
		this.x *= vx;
		this.y *= vy;
		return this;
	}
	public Vector2 mul(double scalar)
	{
		return this.mul(scalar, scalar);
	}
	public Vector2 mul(Vector2 vector)
	{
		return this.mul(vector.x, vector.y);
	}
	public Vector2 mulr(double vx, double vy)
	{
		Vector2 v = new Vector2(this);
		v.mul(vx, vy);
		return v;
	}
	public Vector2 mulr(double scalar)
	{
		Vector2 v = new Vector2(this);
		v.mul(scalar);
		return v;
	}
	public Vector2 mulr(Vector2 vector)
	{
		Vector2 v = new Vector2(this);
		v.mul(vector);
		return v;
	}

	public Vector2 div(double vx, double vy)
	{
		this.x /= vx;
		this.y /= vy;
		return this;
	}
	public Vector2 div(double scalar)
	{
		return this.div(scalar, scalar);
	}
	public Vector2 div(Vector2 vector)
	{
		return this.div(vector.x, vector.y);
	}
	public Vector2 divr(double vx, double vy)
	{
		Vector2 v = new Vector2(this);
		v.div(vx, vy);
		return v;
	}
	public Vector2 divr(double scalar)
	{
		Vector2 v = new Vector2(this);
		v.div(scalar);
		return v;
	}
	public Vector2 divr(Vector2 vector)
	{
		Vector2 v = new Vector2(this);
		v.div(vector);
		return v;
	}
	public Vector2 neg()
	{
		this.x *= -1;
		this.y *= -1;
		return this;
	}
	public Vector2 negr()
	{
		Vector2 v = new Vector2(this);
		v.neg();
		return v;
	}
	public double lenght()
	{
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	public Vector2 normalize()
	{
		double l = this.lenght();
		if(l != 0)
		{
			this.x = x / l;
			this.y = y / l;
		}
		return this;
	}
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(x);
		sb.append(", ");
		sb.append(y);
		sb.append("]");
		return sb.toString();
	}

	public static double dot(Vector2 a, Vector2 b)
	{
		return a.x * b.x + a.y * b.y;
	}
	public double lengthSq()
	{
		return x * x + y * y;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Vector2 other = (Vector2) obj;
		if(Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if(Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

}
