package com.shiro.physics;

public class Rectangle extends Shape
{
	private double width;
	private double height;

	public Rectangle(double width, double height)
	{
		this.width = width;
		this.height = height;
	}
	@Override
	public double calculateMass(double density)
	{
		return width * height * density;
	}
	public Vector2 getCenter()
	{
		return new Vector2(body.position.x + width / 2, body.position.y + height / 2);
	}
	public Vector2 getMin()
	{
		return new Vector2(body.position.x, body.position.y);
	}
	public Vector2 getMax()
	{
		return new Vector2(body.position.x + width, body.position.y + height);
	}
	public double getHeight()
	{
		return height;
	}
	public double getWidth()
	{
		return width;
	}
	@Override
	public Type getType()
	{
		return Type.Rectangle;
	}
}
