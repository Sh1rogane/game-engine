package com.shiro.physics;

public abstract class Shape
{
	public enum Type
	{
		Rectangle, Circle;
	}

	public Body body;

	public abstract double calculateMass(double density);

	public abstract Type getType();
	
	public abstract Vector2 getCenter();
	
	public abstract Vector2 getMin();
	
	public abstract Vector2 getMax();
}
