package com.shiro.physics;

public class Body
{
	public Vector2 position = new Vector2();
	public Vector2 velocity = new Vector2();

	public Shape shape;

	public double mass;
	public double invMass = 0;
	public double restitution = 1;
	public boolean _static;

	public double staticFriction = 0.5;
	public double dynamicFriction = 0.3;

	public Body(Shape shape, double x, double y, double friction, double restitution, boolean _static)
	{
		position.set(x, y);
		setRestitution(restitution);
		setFriction(friction);
		this._static = _static;

		this.shape = shape;
		this.shape.body = this;

		this.mass = shape.calculateMass(1);
		if(!_static)
			this.invMass = 1 / mass;
	}
	public void setRestitution(double restitution)
	{
		this.restitution = restitution;
	}
	public void setFriction(double friction)
	{
		if(friction == 0)
		{
			this.staticFriction = 0;
			this.dynamicFriction = 0;
		}
		else
		{
			this.staticFriction = friction;
			this.dynamicFriction = friction - 0.2;
		}
		
	}
	public void setVelocity(Vector2 velocity)
	{
		this.velocity.set(velocity);
	}
	public void setVelocity(double x, double y)
	{
		this.velocity.set(x, y);
	}
	public void setVelocityX(double x)
	{
		this.velocity.x = x;
	}
	public void setVelocityY(double y)
	{
		this.velocity.y = y;
	}
	public Vector2 getVelocity()
	{
		return this.velocity;
	}
	public void setMass(double d)
	{
		if(d <= 0)
		{
			this._static = true;
			this.invMass = 0;
		}
		else
		{
			this._static = false;
			this.mass = shape.calculateMass(1);
			this.invMass = 1 / mass;
		}
	}
	public void moveTo(Vector2 position)
	{
		this.position.set(position);
	}
	public void moveTo(double x, double y)
	{
		this.position.set(x, y);
	}
	public void applyImpulse(Vector2 impulse)
	{
		this.velocity.add(impulse);
	}
	public void applyImpulse(double vx, double vy)
	{
		this.velocity.add(vx, vy);
	}
	public void resetVelocity()
	{
		this.velocity.reset();
	}
	public void setX(double x)
	{
		this.position.x = x;
	}
	public void setY(double y)
	{
		this.position.y = y;
	}
	public double getX()
	{
		return this.position.x;
	}
	public double getY()
	{
		return this.position.y;
	}
}
