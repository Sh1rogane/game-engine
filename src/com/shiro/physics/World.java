package com.shiro.physics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import com.shiro.physics.Shape.Type;

public class World
{
	private ArrayList<Body> bodies = new ArrayList<>();
	private ArrayList<Body> addBodies = new ArrayList<>();
	private ArrayList<Body> removeBodies = new ArrayList<>();
	private double ac;
	private double frameStart;

	private Vector2 gravity = new Vector2(0, 600);

	private double timeScale = 1;

	public World()
	{
	}
	public void setGravity(Vector2 gravity)
	{
		this.gravity = gravity;
	}
	public void setGravity(double xGravity, double yGravity)
	{
		this.gravity.set(xGravity, yGravity);
	}
	public void setTimeScale(double timeScale)
	{
		this.timeScale = timeScale;
	}
	public void addBody(Body body)
	{
		addBodies.add(body);
	}
	public void removeBody(Body body)
	{
		removeBodies.add(body);
	}
	private void removeAdd()
	{
		bodies.addAll(addBodies);
		addBodies.clear();
		bodies.removeAll(removeBodies);
		removeBodies.clear();
	}
	public void clearBodys()
	{
		bodies.clear();
	}
	public void update(double step)
	{
		double dt = 1.0 / step;
		double currentFrame = System.nanoTime() / 1000000000.0;

		ac += currentFrame - frameStart;

		frameStart = currentFrame;

		if(ac >= 0.2)
		{
			ac = 0.2;
		}

		while(ac >= dt)
		{
			this.tick(dt * timeScale);
			ac -= dt;
		}
	}
	private void integrateForce(Body b, double dt)
	{
		if(!b._static)
		{
			b.velocity.add(gravity.mulr(dt / 2.0));
		}
		else
		{
			b.velocity.reset();
		}
	}
	private void integrateVelocity(Body b, double dt)
	{
		if(!b._static)
		{
			b.position.add(b.velocity.mulr(dt));
			integrateForce(b, dt);
		}
		else
		{
			b.velocity.reset();
		}
	}
	private void tick(double dt)
	{
		removeAdd();
		
//		quadtree.clear();
//		
//		for(int i = 0; i < bodies.size(); ++i)
//		{
//			quadtree.insert(bodies.get(i));
//		}
//		for(int z = 0; z < bodies.size(); ++z)
//		{
//			ArrayList<Body> list = new ArrayList<>();
//			quadtree.retrieve(list, bodies.get(z));
//			for(int i = 0; i < list.size(); ++i)
//			{
//				Body a = list.get(i);
//				for(int j = i + 1; j < list.size(); ++j)
//				{
//					Body b = list.get(j);
//					if(a != b)
//					{
//						if(a._static && b._static)
//							continue;
//
//						Manifold m = null;
//						if(a.shape.getType() == Type.Rectangle && b.shape.getType() == Type.Rectangle)
//						{
//							m = RectVSRect(a, b);
//						}
//						else if(a.shape.getType() == Type.Circle && b.shape.getType() == Type.Circle)
//						{
//							m = CircleVSCircle(a, b);
//						}
//						else if(a.shape.getType() == Type.Rectangle && b.shape.getType() == Type.Circle)
//						{
//							m = RectVSCircle(a, b);
//						}
//						else if(a.shape.getType() == Type.Circle && b.shape.getType() == Type.Rectangle)
//						{
//							m = CircleVSRect(a, b);
//						}
//						if(m != null)
//						{
//							m.solve();
//						}
//					}
//				}
//			}
//		}
//		for(int x = 0; x < 20; ++x)
//		{
			for(int i = 0; i < bodies.size(); ++i)
			{
				Body a = bodies.get(i);
				for(int j = i + 1; j < bodies.size(); ++j)
				{
					Body b = bodies.get(j);
					if(a != b)
					{
						if(a._static && b._static)
							continue;

						Manifold m = null;
						if(a.shape.getType() == Type.Rectangle && b.shape.getType() == Type.Rectangle)
						{
							m = RectVSRect(a, b);
						}
						else if(a.shape.getType() == Type.Circle && b.shape.getType() == Type.Circle)
						{
							m = CircleVSCircle(a, b);
						}
						else if(a.shape.getType() == Type.Rectangle && b.shape.getType() == Type.Circle)
						{
							m = RectVSCircle(a, b);
						}
						else if(a.shape.getType() == Type.Circle && b.shape.getType() == Type.Rectangle)
						{
							m = CircleVSRect(a, b);
						}
						if(m != null)
						{
							m.solve();
						}
					}
				}
			}
//		}
		for(Body b : bodies)
			integrateForce(b, dt);
		for(Body b : bodies)
			integrateVelocity(b, dt);
	}
	private Manifold RectVSRect(Body a, Body b)
	{
		Manifold m = new Manifold();
		m.a = a;
		m.b = b;

		Rectangle aa = (Rectangle) a.shape;
		Rectangle bb = (Rectangle) b.shape;

		Vector2 n = bb.getCenter().subr(aa.getCenter());

		double aExtent = (aa.getMax().x - aa.getMin().x) / 2;
		double bExtent = (bb.getMax().x - bb.getMin().x) / 2;
		double xOverlap = aExtent + bExtent - Math.abs(n.x);

		if(xOverlap > 0)
		{
			aExtent = (aa.getMax().y - aa.getMin().y) / 2;
			bExtent = (bb.getMax().y - bb.getMin().y) / 2;
			double yOverlap = aExtent + bExtent - Math.abs(n.y);
			// How much they overlap in y-axis
			if(yOverlap > 0)
			{
				// If x overlap is greater than y overlap
				if(xOverlap < yOverlap)
				{
					// If collision is on the right side
					if(n.x < 0)
					{
						m.normal = new Vector2(-1, 0);
					}
					// Else left side
					else
					{
						m.normal = new Vector2(1, 0);
					}
					m.penetration = xOverlap;
					return m;
				}
				else
				{
					// If collision is on top
					if(n.y < 0)
					{
						m.normal = new Vector2(0, -1);
					}
					// Else on bottom
					else
					{
						m.normal = new Vector2(0, 1);
					}
					m.penetration = yOverlap;
					return m;
				}
			}
		}
		return null;
	}
	private Manifold CircleVSCircle(Body a, Body b)
	{
		Manifold m = new Manifold();
		m.a = a;
		m.b = b;

		Circle aa = (Circle) a.shape;
		Circle bb = (Circle) b.shape;

		Vector2 normal = bb.getCenter().subr(aa.getCenter());

		double dist_sqr = normal.lengthSq();
		double radius = aa.radius + bb.radius;

		// Not in contact
		if(dist_sqr >= radius * radius)
		{
			return null;
		}

		double distance = (float) StrictMath.sqrt(dist_sqr);

		if(distance == 0.0f)
		{
			m.penetration = aa.radius;
			m.normal.set(1.0f, 0.0f);
		}
		else
		{
			m.penetration = radius - distance;
			m.normal.set(normal).div(distance);
		}
		return m;
	}
	private Manifold RectVSCircle(Body a, Body b)
	{
		Manifold m = new Manifold();
		m.a = a;
		m.b = b;

		Rectangle aa = (Rectangle) a.shape;
		Circle bb = (Circle) b.shape;

		// Vector2 normal = bb.getPosition().subr(a.position);
		Vector2 normal = bb.getCenter().subr(aa.getCenter());

		Vector2 closest = normal.copy();

		double x_extent = (aa.getMax().x - aa.getMin().x) / 2.0;
		double y_extent = (aa.getMax().y - aa.getMin().y) / 2.0;

		closest.x = clamp(-x_extent, x_extent, closest.x);
		closest.y = clamp(-y_extent, y_extent, closest.y);

		boolean inside = false;

		// System.out.println(normal);
		// System.out.println(closest);

		if(normal.equals(closest))
		{
			inside = true;
			// Find closest axis
			if(Math.abs(normal.x) > Math.abs(normal.y))
			{
				// Clamp to closest extent
				if(closest.x > 0)
					closest.x = x_extent;
				else
					closest.x = -x_extent;
			}
			else
			{
				// Clamp to closest extent
				if(closest.y > 0)
					closest.y = y_extent;
				else
					closest.y = -y_extent;
			}
		}

		Vector2 n = normal.subr(closest);

		double d = n.lengthSq();
		double r = bb.radius;

		if(d > r * r && !inside)
			return null;

		d = Math.sqrt(d);

		if(inside)
		{
			m.normal = n.negr().normalize();
			m.penetration = r - d;
		}
		else
		{
			m.normal = n.copy().normalize();
			m.penetration = r - d;
		}
		// System.out.println(d);
		return m;
	}
	private Manifold CircleVSRect(Body a, Body b)
	{
		// Creates Manifold and sets bodies
		Manifold m = new Manifold();
		m.a = a;
		m.b = b;

		Circle aa = (Circle) a.shape;
		Rectangle bb = (Rectangle) b.shape;

		Vector2 normal = bb.getCenter().subr(aa.getCenter());

		Vector2 closest = normal.copy();

		double x_extent = (bb.getMax().x - bb.getMin().x) / 2.0;
		double y_extent = (bb.getMax().y - bb.getMin().y) / 2.0;

		closest.x = clamp(-x_extent, x_extent, closest.x);
		closest.y = clamp(-y_extent, y_extent, closest.y);

		boolean inside = false;

		if(normal.equals(closest))
		{
			inside = true;
			// Find closest axis
			if(Math.abs(normal.x) > Math.abs(normal.y))
			{
				// Clamp to closest extent
				if(closest.x > 0)
					closest.x = x_extent;
				else
					closest.x = -x_extent;
			}
			else
			{
				// Clamp to closest extent
				if(closest.y > 0)
					closest.y = y_extent;
				else
					closest.y = -y_extent;
			}
		}

		Vector2 n = normal.subr(closest);
		double d = n.lengthSq();
		double r = aa.radius;

		if(d > r * r && !inside)
			return null;

		d = Math.sqrt(d);

		if(inside)
		{
			m.normal = n.negr().normalize();
			m.penetration = r - d;
		}
		else
		{
			m.normal = n.copy().normalize();
			m.penetration = r - d;
		}
		return m;
	}
	private static double clamp(double min, double max, double a)
	{
		return (a < min ? min : (a > max ? max : a));
	}
	public void debugDraw(Graphics2D g, Color color)
	{
		g.setColor(color);
		// g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		// RenderingHints.VALUE_ANTIALIAS_ON);
		for(Body b : bodies)
		{
			AffineTransform af = g.getTransform();

			g.translate(b.position.x, b.position.y);
			if(b.shape.getType() == Type.Rectangle)
			{
				Rectangle r = (Rectangle) b.shape;
				g.drawRect(0, 0, (int) r.getWidth(), (int) r.getHeight());
			}
			else if(b.shape.getType() == Type.Circle)
			{
				Circle r = (Circle) b.shape;
				g.drawOval(0, 0, (int) (r.radius * 2), (int) (r.radius * 2));
				g.drawLine((int) r.radius, (int) r.radius, (int) (r.radius + r.radius), (int) r.radius);
				g.fillOval((int) r.radius - 3, (int) r.radius - 3, 6, 6);
			}

			g.setTransform(af);
		}
	}
}
