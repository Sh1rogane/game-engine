package com.shiro.physics;

public class Manifold
{
	public Body a;
	public Body b;
	public double penetration;
	public Vector2 normal = new Vector2();

	public void solve()
	{
		// Do not solve for 2 static bodys
		if(a._static && b._static)
			return;

		Vector2 rv = b.velocity.subr(a.velocity);

		double velAlongNormal = Vector2.dot(rv, this.normal);
		if(velAlongNormal > 0)
			return;

		double e = Math.min(this.a.restitution, this.b.restitution);

		// impulse scalar
		double j = -(1 + e) * velAlongNormal;
		j /= this.a.invMass + this.b.invMass;

		Vector2 impulse = normal.mulr(j);

		this.a.velocity.sub(impulse.mulr(a.invMass));
		this.b.velocity.add(impulse.mulr(b.invMass));

		rv = b.velocity.subr(a.velocity);
		// Solve for the tangent vector
		Vector2 tangent = rv.copy();
		tangent.sub(normal.mulr(-Vector2.dot(rv, normal)));
		tangent.normalize();

		// Solve for magnitude to apply along the friction vector
		double jt = -Vector2.dot(rv, tangent);
		jt /= a.invMass + b.invMass;

		// System.out.println(jt);
		double sf = Math.sqrt(a.staticFriction * b.staticFriction);

		// Clamp magnitude of friction and create impulse vector
		Vector2 frictionImpulse = new Vector2();
		if(Math.abs(jt) < j * sf)
		{
			frictionImpulse = tangent.mulr(jt);
		}
		else
		{
			double df = Math.sqrt(a.dynamicFriction * b.dynamicFriction);
			frictionImpulse = tangent.mulr(df * -j);
		}

		// Apply
		a.velocity.sub(frictionImpulse.mulr(a.invMass));
		b.velocity.add(frictionImpulse.mulr(b.invMass));

		// correction
		double percent = 0.8;
		double slop = 0.02;

		Vector2 correction = normal.mulr(percent).mulr(
				Math.max(this.penetration - slop, 0.0) / (a.invMass + b.invMass));

		this.a.position.sub(correction.mulr(a.invMass));
		this.b.position.add(correction.mulr(b.invMass));
	}
}
